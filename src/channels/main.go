package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://www.rosatti.com.ar",
		"http://stackoverflow.com",
		"http://golang.org",
		"http://amazon.com",
	}

	//Create our first channel
	c := make(chan string) //chan is the key word for channels, string is the type of data we'll be sending

	//Send Message
	// c <- "data"
	//Receive Message
	// var a string
	// a <- c

	for _, link := range links {
		go checkLink(link, c)
	}

	//Infinite Loop
	for {
		l := <-c
		go func(l string) {
			time.Sleep(5 * time.Second)
			checkLink(l, c)
		}(l)
	}

	//Alternative syntax for infinite loop
	/*for l := range c {
		go func() {
			time.Sleep(5 * time.Second)
			checkLink(l, c)
		}()
	}*/
}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, " might be down!")
	} else {
		fmt.Println(link, " is up!")
	}

	c <- link
}
