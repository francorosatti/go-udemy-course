package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

//In this project we will:
// 1. Make an http request to google.com
// 2. Print response in the console
// 3. Create custom writer

type logWriter struct{}

func main() {
	// Step 1: Make an http GET
	resp, err1 := http.Get("http://google.com")
	// Step 2: Check for errors
	if err1 != nil {
		fmt.Println("Error1:", err1)
		os.Exit(1)
	}

	method := 3

	if method == 1 {
		// METHOD 1: Reading the body manually and printing
		fmt.Println("METHOD 1")

		bs := make([]byte, 99999999)  //create a byte slice of huge size just in case
		_, err2 := resp.Body.Read(bs) //actually call the read function
		//check for errors
		if err2 != nil && err2 != io.EOF {
			fmt.Println("Error2:", err2)
			os.Exit(1)
		}
		//print response
		fmt.Println(string(bs))
	} else if method == 2 {
		// METHOD 2: Use io.Copy that takes an implementation of Reader and Writer to copy

		//As resp.Body implements Reader, it can be used in other functions
		// like io.Copy, that copies the content of the resp.Body Reader to
		// the Stdout
		fmt.Println("METHOD 2A")
		io.Copy(os.Stdout, resp.Body) //Writer, Reader

	} else if method == 3 {
		// METHOD 2: Use io.Copy that takes an implementation of Reader and Writer to copy

		//Create a new type that implements a Writer
		lw := logWriter{}

		//We can also copy to our own WRITER
		fmt.Println("METHOD 2B")
		io.Copy(lw, resp.Body) //Writer, Reader
	}

	//END OF PROGRAM
	resp.Body.Close()
}

//implementation of interface WRITER
func (logWriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("Bytes written:", len(bs))
	return len(bs), nil
}
