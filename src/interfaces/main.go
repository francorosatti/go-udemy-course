package main

import "fmt"

//IBot The interface will be applied to any type that implements averything in the interface
//That's why englishBot and spanishBot both are treated as type bot, cause they implement the interface
//It works similarly to .NET but there is no need to declare that a type implements an interface, it just do it and that's it
type IBot interface {
	getGreeting() string
}

type englishBot struct{}
type spanishBot struct{}

func main() {
	eb := englishBot{}
	sb := spanishBot{}

	printGreeting(eb)
	printGreeting(sb)
}

func printGreeting(b IBot) {
	fmt.Println(b.getGreeting())
}

func (englishBot) getGreeting() string { //if we are not using the receiver a can just put only the type
	//Imagine the logic for both functions is very complicated and different
	return "Hello!"
}

func (spanishBot) getGreeting() string {
	return "Hola!"
}
