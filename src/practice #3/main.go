package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	//Check Arguments
	if len(os.Args) < 2 {
		fmt.Println("Error en los parametros: go run main.go <filename>")
		os.Exit(1)
	}

	//Start Program
	filename := os.Args[1]
	fmt.Println("-------------------------------------")
	fmt.Println("Welcome to GO cat emulator")
	fmt.Println("-------------------------------------")
	fmt.Println("")
	fmt.Println("File Name:")
	fmt.Println(filename)
	fmt.Println("")

	//Open File
	fp, err := os.Open(filename)
	if err != nil {
		fmt.Println("Couldn't open file", filename)
		os.Exit(1)
	}

	//Read & Log File
	fmt.Println("File Content:")
	io.Copy(os.Stdout, fp)
	fmt.Println("")

	//End of Program
	fmt.Println("")
	fmt.Println("-------------------------------------")

	fp.Close()
}
