package main

import "fmt"

func (p person) print() {
	fmt.Println(p.firstName, p.lastName, "is", p.age)
}

type person struct {
	firstName string
	lastName  string
	age       int
	contact   contactInfo
}

type contactInfo struct {
	email string
	zip   int
}

func main() {
	alex := person{
		"Alex",
		"Anderson",
		25,
		contactInfo{
			email: "alex@alex.com",
			zip:   31025,
		},
	}
	alex.print()
	fmt.Printf("%+v\n", alex)

	fran := person{age: 30, lastName: "Rosatti", firstName: "Fran"}
	fran.print()
	fmt.Println(fran)

	var sole person
	sole.firstName = "sole"
	fmt.Printf("%+v\n", sole)
	sole.updateName1("ANA SOLEDAD")
	fmt.Printf("%+v\n", sole)

	//this is the classic way of using pointers
	solePtr := &sole
	solePtr.updateName2("SOLECITA")
	sole.print()

	//this is the short way
	fran.print()
	fran.updateAge(35) //we call a pointer receiver fun with actual object
	fran.print()

	name := "Lorenzo"
	fmt.Println(*&name)

	//Pointer are needed for Value Types as strings, ints, structs, etc
	//When we are using Slices, as they are Reference Types, Go will pass them
	// always as pointers.
}

//we can call this function with a person or a pointer to a person
func (p *person) updateAge(newAge int) {
	(*p).age = newAge //me use p directly as a pointer
}

//sends p by reference
func (p *person) updateName2(newFirstName string) {
	(*p).firstName = newFirstName
}

//sends p by value
func (p person) updateName1(newFirstName string) {
	p.firstName = newFirstName
}
