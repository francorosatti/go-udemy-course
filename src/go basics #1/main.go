package main

import "fmt"

// we can declare variables outside a function
var user string

// but we can't assing a value outside function this way
//temp := "hello"

//however is possible doing this
var count int = 8

func main() {
	// defining variables in different ways
	var card1 string = "Ace of Spades"
	card1 = getNewCard()
	card2 := "Ace of Spades"   //The same, compiler will infer this a string variable
	card2 = "Five of Diamonds" //Assing a value

	fmt.Println(card1)
	fmt.Println(card2)

	//first create a variable and then assign a value
	var num int
	num = 5
	fmt.Println(num)

	fmt.Println(count)

	// This function is defined in another file
	// In order to use it, we must do: "go run main.go state.go"
	printState()

	/*********** ARRAYS & SLICES *************/
	/*
		An array is a fixed-length list of elements
		A slice can grow or shrink, is fancier

		Both are strong typed, all elements are the same type
	*/

	cards := []string{"Ace of Spades", getNewCard()} //Create a slice
	cards = append(cards, "Six of Diamonds")         //Add a new card
	fmt.Println(cards)

	// Iterate over cards in slice
	for i, card := range cards {
		fmt.Println(i, card)
	}
}

func getNewCard() string { //we can avoid the return type if function does not return a value
	return "Eight of Hearts"
}
