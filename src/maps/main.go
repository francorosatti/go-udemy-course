package main

import "fmt"

func main() {
	//map where keys are type string (between square brackets) and values are also strings
	colors := map[string]string{
		"red":   "#ff0000",
		"green": "#00ff00",
		"blue":  "#0000ff",
	}
	colors["white"] = "#ffffff"
	fmt.Println(colors)
	delete(colors, "green")
	fmt.Println(colors)

	cities := make(map[string]string)
	cities["AR"] = "CABA"
	fmt.Println(cities)

	var animals map[string]string
	//animals["cat"] = "best"
	fmt.Println(animals)

	//Iterate over a map
	printMap(colors)
}

func printMap(c map[string]string) {
	//Iterate over a map
	for key, value := range c {
		fmt.Println("Hex color for", key, "is", value)
	}
}
