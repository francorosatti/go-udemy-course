package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

// Create a new type of data
// type is the keyword to define a new type
// deck is the name we give to this new type
// []string is the base type we inherit from
type deck []string

func newDeck() deck {
	cards := deck{}
	suites := []string{"Spades", "Clubs", "Hearts", "Diamonds"}
	values := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Joker", "Queen", "King"}
	for _, suite := range suites {
		for _, value := range values {
			cards = append(cards, value+" of "+suite)
		}
	}
	return cards
}

// This is a receiver function, is a function we add to the type 'deck'
func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	//slice an array => v[5:7] gets the sub sets including v[5] and v[6], it excludes v[7]
	hand := d[:handSize] //when omiting first parameter it starts at 0
	rest := d[handSize:] //when omiting second parameter it goes to end of array
	return hand, rest
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

func (d deck) saveToFile(filename string) error {
	//type conversion
	//var bytes []byte = []byte("Helo, World!")
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func newDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1) //If I can't read from file, program ends
	}

	return deck(strings.Split(string(bs), ","))
}

func (d deck) shuffle() {
	//create a seed for randomize
	rand.Seed(time.Now().UnixNano())
	for i := range d {
		//generate random position
		rnd := rand.Intn(len(d))

		//swap old-school way
		//rc := d[rnd]
		//d[rnd] = d[i]
		//d[i] = rc

		//swap with fancy syntax
		d[i], d[rnd] = d[rnd], d[i]
	}
}
