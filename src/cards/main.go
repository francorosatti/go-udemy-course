package main

func main() {
	cards := newDeck()
	cards.print()
	//hand, _ := cards.deal(5)

	//save deck to file
	cards.saveToFile("myCards.txt")

	//read deck from file
	cards2 := newDeckFromFile("myCards.txt")
	cards2.shuffle()
	cards2.print()
}
